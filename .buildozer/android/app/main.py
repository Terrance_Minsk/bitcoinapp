from kivy.app import App
from kivy.lang import Builder
from kivy.graphics import Color, Rectangle
from kivy.uix.pagelayout import PageLayout
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.network.urlrequest import UrlRequest
import urllib
from kivy.properties import ObjectProperty
import json
import urllib2
import requests
from kivy.uix.button import Button


data_dir = ''



class ScreenOne(Screen):
    _username = ObjectProperty()
    _password = ObjectProperty()

    def on_pre_enter(self, *args):
        pass

    def on_enter(self, *args):
        self.rebuild_grid()

    def rebuild_grid(self):
        if hasattr(self, "label1"):
            print(self.label1.text)

        if hasattr(self, "label2"):
            print(self.label2.text)

        pass

    def on_leave(self, *args):
        # my_box1 = BoxLayout(orientation='vertical')
        
        # Label1= Label(text="Username",font_size='24dp')
        # TextInput1= TextInput(multiline=False)
        #
        # Label2= Label(text="Email",font_size='24dp')
        # TextInput2= TextInput(multiline=False)
        #
        # Label3= Label(text="Password",font_size='24dp')
        # TextInput3= TextInput(multiline=False, password=True)

        # my_button1 = Button(text="ENTER", size_hint_y=None, size_y=100)
        # my_button1.bind(on_press=lambda x: self.activ(TextInput1.text, TextInput3.text))
        # my_button1.bind(on_press=self.changer)

        # my_box1.add_widget (Label1)
        # my_box1.add_widget (TextInput1)
        # my_box1.add_widget (Label2)
        # my_box1.add_widget (TextInput2)
        # my_box1.add_widget (Label3)
        # my_box1.add_widget (TextInput3)
        # my_box1.add_widget(my_button1)
        # self.add_widget(my_box1)
        pass

    def changer(self,*args):
        self.manager.current = 'screen2'    

    def activ(self, login, password):
        print(login)
        print(password)
    
        def success(request, result):
            # print(result)

            self.do_auth(login, password)
     
        def fail(request, error):
            print(error)
     
        def failure(request, error):
            print(error)



        url = 'http://46.101.138.26:8080/u/r/'
        method = 'GET'

        headers = {
            'Content-type': 'application/json',
            'Accept': 'application/json'
            }

        url +='?username=' + str(login) + '&password=' + str(password)
        # params= urllib.urlencode(
        #     {'@username':login, '@password':password }
        # )

        try:
            UrlRequest(
                url, method=method, req_headers=headers,
                on_success=success, on_error=fail, on_failure=failure, debug=True,
                timeout=5
            )
        except Exception as e:
            print(e)

    def do_auth(self, login, password):

        def success(request, result):
            # print(result)
            f = open("output.txt", 'w')
            f.write(result)
            f.close()

        def fail(request, error):
            print(error)

        def failure(request, error):
            print(error)



        #     data = '{"nw_src": "10.0.0.1/32", "nw_dst": "10.0.0.2/32", "nw_proto": "ICMP", "actions": "ALLOW", "priority": "10"}'
        # url = 'http://localhost:8080/firewall/rules/0000000000000001'
        # req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
        # f = urllib2.urlopen(req)
        # for x in f:
        #     print(x)
        # f.close()

        url = 'http://46.101.138.26:8080/o/token/'
        method = 'POST'

        headers = {
            'Content-type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            }


        body = json.dumps({
            "grant_type": "password",
            "username": login,
            "password": password,
            "client_id": "rest",
            "client_secret": "rest"
        })

        print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        print(body)

        try:
            UrlRequest(
                url, method=method, req_headers=headers, req_body=body,
                on_success=success, on_error=fail, on_failure=failure, debug=True,
                timeout=5
            )
        except Exception as e:
            print(e)


 
class ScreenTwo(Screen,PageLayout):

    _costumestring=ObjectProperty()
    def on_pre_enter(self, *args):
        pass

    def on_enter(self, *args):
        Clock.schedule_interval(self.call_urls, 5)
        self.rebuild_grid()

    def rebuild_grid(self):
        
        pass
        
    def on_leave(self, *args):
     
        pass


 
    def call_urls(self, sdf, **kwargs):
        print '2' 

        s = requests.Session()
        x = s.post('http://www.satoshigarden.org/')
        e =("Satoshi:") + str(x) + ("\n")

        
        with open('urls.json') as data_file:
            data = json.load(data_file)
        urls_array = data["urls"]
        kol=len( urls_array)
        kol=str(kol)
        with open('zaprosi.json', "w") as outfile:
            json.dump({'strings':kol}, outfile, indent=1)  
        
        strings=("Requests: ") + (kol) + ( "\n")
        i = 0
        custom_string = (strings) + (e)
        for url in urls_array:
            i += 1
            temp = urllib2.urlopen(url)
            custom_string += ("{0}: {1}: {2} OK ".format((i),url, temp.getcode())+("\n"))
        my_box1 = BoxLayout(orientation='vertical')
        Label1= Label(text=custom_string,font_size='20dp',halign='center')
        my_box1.add_widget (Label1)
        self.add_widget(my_box1)

 

 
class MyApp(App):
 
    def build(self):
        my_screenmanager = ScreenManager()
        Builder.load_file('my.kv')
        screen1 = ScreenOne(name='screen1')
        screen2 = ScreenTwo(name='screen2')
        my_screenmanager.add_widget(screen1)
        my_screenmanager.add_widget(screen2)
        return my_screenmanager
        
        
        

        
 
__version__ = '0.2'

if __name__ == '__main__':
    MyApp().run()




    # url = 'http://46.101.138.26:8080/o/token/'
    # method = 'POST'
    #
    # headers = {
    #     'Content-type': 'application/json',
    #     'Accept': 'application/json'
    #     }
    #
    # body = json.dumps({
    #     "username": login,
    #     "password": password,
    #     "client_id": "prosto",
    #     "client_secret": "super_prosto"
    # })
    #
    # try:
    #     UrlRequest(
    #         url, method=method, req_headers=headers, req_body=body,
    #         on_success=success, on_error=fail, on_failure=failure, debug=settings.URL_REQUEST_DEBUG,
    #         timeout=settings.URL_REQUEST_TIMEOUT
    #     )
    # except Exception as e:
    #     print(e)